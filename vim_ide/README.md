# Vim as a IDE

## About

Vim is a powerful text editor.  Vim can be configured to be an effective
Integrated Development Environment (IDE).  IDEs make programming easier.

Provided here are two 'levels' of IDE for VIM which are named Simple and
Improved.

This project was created on vim 8.2.  It is recommended to use that version
or higher.


## Simple IDE

The Simple IDE uses basic vim commands and short scripts to provide a
quick enviroment for programming.

To setup this IDE

A. Add the vim color scheme to the local environment
  1. Create the directory if it doesn't exist

  `$ mkdir ~/.vim/colors/`

  2. Copy gruvbox to the directory

  `$ cp gruvbox.vim ~/.vim/colors/`

B. Copy the Vim configuration file to your home directory
  1. Backup any existing configuration

  `$ cp ~/.vimrc ~/.vimrc.bak`

  2. Copy the simple ide configuration

  `$ cp vimrc.simple ~/.vimrc`

### Screenshot of vimrc.simple in action

![vimrc-simple.png](https://gitlab.com/JasonCoke/metallic_shadows/-/raw/master/vim_ide/images/vimrc-simple.png)



## Improved IDE

The improved IDE concept uses plugins to provide an IDE.  These plugins
required some type of plugin manager.  Vundle was choosen but several other
exist.

A. Install Vundle
   1. Site w/ Instructions: https://github.com/VundleVim/Vundle.vim
   2. Basic install instructions include git clone, adding lines to .vimrc,
      and running a InstallPlugin command

B. Install plugins
   1. YouCompleteMe - Autocomplete
   2. Nerdtree - File explorer
   3. Vim-rainbow - Bracket/Braces coloring
   4. Nerdcommenter - Adds commented lines
   5. Vim-airline - Fancy status line
   6. Auto-pairs - Auto insert of quotes and brackets
   7. Syntastic - Syntax checker

C. Plugins details
   1. Plugins were installed ~/.vim/bundle
   2. Several plugins had extra packages or configuration that were needed

### Screenshot of vimrc.improved in action
![vim-improved.png](https://gitlab.com/JasonCoke/metallic_shadows/-/raw/master/vim_ide/images/vim-improved.png)

## Files
| Name | Description |
|----- |----- |
| code.c         | Sample c code |
| gruvbox.vim    | Recommend vim scheme |
| images         | Image directory   |
| notes.txt      | Notes used for the presentation |
| README.md      | This file |
| vimrc.combo    | Simple and improved vimrc combined |
| vimrc.improved | Plugin enabled vim IDE |
| vimrc.simple   | Simple vim IDE |

