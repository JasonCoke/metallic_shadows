#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    const int value1 = 0;  // Value of thing one
    int value2 = 1;        // Value of thing two
    void *my_void_pointer = NULL;

    // TODO: Write purfect code
    printf("Some code should be written here\n");
    my_void_pointer = &value2;
    print("This");

    /* Loop though items
     */
    for (int i = 0; i < value2; i++)
    {
        if (100 == i)
        {
		    printf("%i\n", i);
		    if (99 == i)
		    {
		        printf("%i\n", i);
		    }
        }
    }

    return value2;
}

