set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

Plugin 'YouCompleteMe'
Plugin 'preservim/nerdtree'
Plugin 'frazrepo/vim-rainbow'
Plugin 'preservim/nerdcommenter'
Plugin 'vim-airline/vim-airline'
Plugin 'jiangmiao/auto-pairs'
Plugin 'syntastic'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Plugin requirements
syntax on
let g:rainbow_active = 1
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']
" Fix issue
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'
" let g:syntastic_c_checkers = ['ClangCheck']
let g:airline_powerline_fonts = 1


" -------------------------------------------------------------------------------
" Simple IDE
" -------------------------------------------------------------------------------

"syntax on                     " Allow color
set termguicolors             " Almost 24-bit colors
set background=dark           " Color theme is dark
colorscheme gruvbox           " Recommend for c programming

set nu                         " Line numbering
set hlsearch                   " Highlight search results
set tabstop=4                  " Tab key is 4 characters wide
set expandtab                  " Tab key inserts spaces
set ai                         " Auto-indenting

hi ColorColumn ctermbg=blue    " Highlight vertical line
set textwidth=100              " Max line width
set colorcolumn=+0             " Show line

set scrolloff=5                " Scroll padding
set backspace=indent,eol,start " Allows deleting prior to insert

set cursorline                 " Highlight cursor line

" Underline the current line and number line
"hi CursorLine cterm=NONE guibg=grey guifg=darkblue
"hi CursorLineNR cterm=bold guibg=blue guifg=lightblue


" Status line
"set laststatus=2
"let b = system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
"set statusline=%{b}
"set statusline+=%2*\ %F\ %m%r[%l,%v]\ [%p%%]
"set statusline+=%1*\ %=
"hi User1 ctermbg=grey ctermfg=green
"hi User2 ctermbg=darkgrey ctermfg=green

set wildmenu                   " Display completion matches in status line

" Remove training whitespace
autocmd BufWritePre * :%s/\s\+$//e

" Return to the line when exited
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

" Spelling highlight
hi SpellBad cterm=underline ctermfg=white ctermbg=red
