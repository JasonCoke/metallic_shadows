#include <stdio.h>
#include <stdlib.h>

void print_msg(void)
{
    printf("Simple Binary - GDB Tutor Program\n");
}


int add(int x)
{
    int y = 0;
    y = x + 1;
    return y;
}

int main(int argc, char *argv[])
{
    int x = 0;

    print_msg();

    if (argc == 2)
       x = atoi(argv[1]);
    else
       x = 1;


    printf("Before x = %x\n", x);

    x = add(x);

    printf("After x = %x\n", x);

    return 0;
}
