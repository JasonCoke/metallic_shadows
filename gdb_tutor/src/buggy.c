/*
 * File: buggy.c
 * Description: A program full of common coding bugs.
 *
 * Details: This program is to be used with the gdb-tutor.txt file in
 *          order to teach gdb fundamentals.
 */
#include "buggy.h"
#include <stdlib.h>


/* ****************************************************************************
 * Desc: Returns the bug lesson number or zero on error.
 * ************************************************************************* */
int get_input(int argc, char **argv)
{
    if (argc == 2)
       return atoi(argv[1]);

    return 0;
}


/* ****************************************************************************
 * Desc: Common syntax issues
 * ************************************************************************* */
void bugs_01(void)
{
    int x = 8;

    printf("Buggy function 05\n");

    printf("Initial value = %i\n", x);

    x = subtract_two(x);

    printf("Final value = %i\n", x);

    return;
}


/* ****************************************************************************
 * Desc: Syntax errors
 * ************************************************************************* */
void bugs_02(void)
{
    int total = 0;
    int a[8] = { 6, 4, 8, 2, 5, 5, 1, 9}; // Totals 40

    printf("Buggy function 02\n");

    // Sum the values of the array
    total = sum(a, 8);

    printf("Total %i\n", total);

    return;
}


/* ****************************************************************************
 * Desc: Buggy conditionals
 * ************************************************************************* */
void bugs_03(void)
{
    int total = 40;
    unsigned int expected_array_total = 40;

    printf("Buggy function 03\n");

    // Check total for array sum
    check_total(total, expected_array_total);

    return;
}


/* ****************************************************************************
 * Desc: Buggy pointers
 * ************************************************************************* */
void bugs_04(void)
{
    int x = 10;
    int *p;

    printf("Buggy function 04\n");

    update_value(x);
    printf("Updated value: %i\n", x);

    p = &x;
    update_value_by_pointer(p);
    printf("Updated value: %i\n", x);

    return;
}


/* ****************************************************************************
 * Desc: Buggy loops
 * ************************************************************************* */
void bugs_05(void)
{
    printf("Buggy function 05\n");

    loopy();

    return;
}


/* ****************************************************************************
 * Desc: Buggy recursion
 * ************************************************************************* */
void bugs_06(void)
{
    int i = 5;  // Factorial should be 120 for 5

    printf("Buggy function 06\n");

    // Find the recursion of i
    printf("Factorial of %d is %d\n", i, factorial(i));

    return;
}


/* ****************************************************************************
 * Desc: Buggy string copying
 * ************************************************************************* */
void bugs_07(void)
{
    char *str1 = "Debugging code is fun";
    char *str2;

    printf("Buggy function 07\n");

    // Copy string 1 to string 2
    duplicate_string(str2, str1);

    return;
}


/* ****************************************************************************
 * Desc: Buggy string compare
 * ************************************************************************* */
void bugs_08(void)
{
    char *str1 = "Debugging code is fun";
    char *str2 = (char *) malloc(sizeof(char) * 22);

    printf("Buggy function 08\n");

    // Strings 1 and 2 are the different
    strcpy(str2, "Debugging code meh");
    compare_strings(str1, str2);

    // Strings 1 and 2 are the same
    strcpy(str2, "Debugging code is fun");
    compare_strings(str1, str2);

    return;
}


/* ****************************************************************************
 * The main program
 *
 * Details: This function contains the entry point into the various buggy
 *          functions.
 *
 * ************************************************************************* */
int main(int argc, char *argv[])
{
    int bug = 0;
    bug = get_input(argc, argv);

    printf("GDB tutorial - A buggy program\n");

    switch(bug)
    {
        case 1: bugs_01(); break;
        case 2: bugs_02(); break;
        case 3: bugs_03(); break;
        case 4: bugs_04(); break;
        case 5: bugs_05(); break;
        case 6: bugs_06(); break;
        case 7: bugs_07(); break;
        case 8: bugs_08(); break;
        default:
            printf("Bug number %s not recognized\n", argv[1]);
            printf("Usage: %s bugNumber\n", argv[0]);
    }

    return 0;
}
