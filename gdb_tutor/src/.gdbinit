define status
  frame
  info args
  info locals
end

set history filename ~/.gdb_history
    set history save on
    set history size 10000

set breakpoint pending on
file fun
set args 2
b main
b fun02
b level1
b level2
r
tui enable
