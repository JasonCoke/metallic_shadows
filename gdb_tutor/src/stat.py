import gdb

class Status(gdb.Command):
    ''' Status command '''
    def __init__(self):
        super().__init__("stat", gdb.COMMAND_USER)

    def invoke(self, args, tty):
        try:
            f = gdb.newest_frame()
            if f.is_valid():
                print("Function name: " + f.name())

            i = gdb.selected_inferior()
            if i.is_valid():
                print("Pid: " + str(i.pid))

            a = i.architecture()
            print("Arch: " + a.name())

            p = i.progspace
            if p.is_valid():
                print("File: " + p.filename)

        except Exception as e:
            print(e)

Status()
