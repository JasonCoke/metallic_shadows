import gdb
class Length (gdb.Function):
    ''' Returns length of a string '''
    def __init__(self):
        super (Length, self).__init__("len")
    def invoke (self, name):
        return "Length: %i" % len(name.string ())
Length ()
