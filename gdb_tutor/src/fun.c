/*
 * File: fun.c
 * Description: A program with functions to facilitate gdb lessons
 *
 * Details: This program is to be used with the gdb-tutor.txt file in
 *          order to teach gdb fundamentals.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>      // sleep
#include <syslog.h>      // openlog, syslog
#include <pthread.h>
#include <signal.h>


#define VIDEOS 3
#define MAX_THREADS 8

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
void *print_info(void *pr);

typedef struct
{
    char month[4];
    int day;
    int year;
} uploaded;


typedef struct
{
    char *title;
    char *author;
    int likes;
    char *link;
    uploaded *date;
} video;


/* ****************************************************************************
 * Desc: A simple vulnerable buffer overflow
 * ************************************************************************* */
int vuln(void)
{
   char buf[8];

   printf("Input: ");

   gets(buf);
   puts(buf);

   return 1;
}


/* ****************************************************************************
 * Desc: Creates two threads
 * ************************************************************************* */
int thread(void)
{
    pthread_t threads[MAX_THREADS];

    for (int i = 0; i < MAX_THREADS; ++i)
    {
        pthread_create(&threads[i], NULL, print_info, NULL);
    }

    for (int i = 0; i < MAX_THREADS; ++i)
    {
        pthread_join(threads[i], NULL);
    }

    return 0;
}


/* ****************************************************************************
 * Desc: Simple function to use as a thread
 * ************************************************************************* */
void *print_info(void *ptr)
{
    pthread_t pt = pthread_self();
    unsigned char *ptc = (unsigned char*) (void*) (&pt);

    pthread_mutex_lock(&mutex1);

    printf("Thread id: %ld:\n", pt);
    printf("Hex value 0x");

    for (size_t i=0; i<sizeof(pt); i++)
    {
        printf("%02x", (unsigned)(ptc[i]));
    }

    printf("\n");
    sleep(1);

    pthread_mutex_unlock(&mutex1);

    return NULL;
}


/* ****************************************************************************
 * Desc: Catches CTRL-C for server() function.
 * ************************************************************************* */
void handle_sig(int sig)
{
    printf("Caught signal %d\n", sig);
}


/* ****************************************************************************
 * Desc: Simple server that forks.  Child prints 3 times to syslog and exits.
 * Child also catches SIGUSR1 just to print a statement.
 * ************************************************************************* */
int server(void)
{
    pid_t pid;
    int count = 0;

    // Parent
    if (0 > (pid = fork()))
        exit(EXIT_FAILURE);

    // Child
    if (0 < pid)
        exit(EXIT_SUCCESS);

    // Signal handling
    signal(SIGUSR1, handle_sig);

    // Syslog
    openlog("Logs", LOG_PID, LOG_USER);

    while(1)
    {
        // Print a message only three times
        if (count < 3)
        {
           syslog(LOG_INFO, "Log logging");
           ++count;
        }
        else
        {
            break;
        }
        sleep(5);
    }

    closelog();
    return 0;
}


/* ****************************************************************************
 * Desc: Level2 function
 * ************************************************************************* */
int level2(int d, int e, int g)
{
    int i = 0;

    i = ++d;
    i += e;
    i -= g;

    return i;
}


/* ****************************************************************************
 * Desc: Level1 function
 * ************************************************************************* */
int level1(int a, int b)
{
    int d = 6;
    int e = 8;
    int g = 9;
    int h;

    h = level2(d, e, g);

    return a + b + h;
}


/* ****************************************************************************
 * Desc: Allocated space and adds video to library
 * ************************************************************************* */
int add_video_to_library(video *index, char *title, char *author, int likes,
                         char *month, int day, int year, char *link)
{
     int ret = 0;

     // Allocate space & assign
     index->title = strdup(title);
     index->author = strdup(author);
     index->link = strdup(link);

     // Allocate space for upload struct & assign
     index->date = (uploaded *) malloc(sizeof(uploaded));
     strcpy(index->date->month, month);

     index->date->day = day;
     index->date->year = year;

     // Assign likes
     index->likes = likes;

     return ret;
}


/* ****************************************************************************
 * Desc: Builds the library
 * ************************************************************************* */
video **build_video_library(void)
{
    video **library;

    // Allocate the library
    library = (video **) malloc(sizeof(video *) * VIDEOS);

    for (int i = 0; i < VIDEOS; i++)
        library[i] = (video *) malloc(sizeof(video));

    // Add video 1 to library
    add_video_to_library(library[0], "Debugging With GDB", "Robert Martin",
                         148, "Jul", 1, 2017,
                         "https://www.youtube.com/watch?v=OpVMB7DNlmY");

    // Add video 2 to library
    add_video_to_library(library[1], "CppCon 2016 GDB", "Greg Law",
                         148, "Oct", 2, 2016,
                         "https://www.youtube.com/watch?v=-n9Fkq1e6sg");

    // Add video 3 to library
    add_video_to_library(library[2], "Debugging - GDB Tutorial", "Chris Bourke",
                         148, "Apr", 19, 2018,
                         "https://www.youtube.com/watch?v=bWH-nL7v5F4");

    return library;
}


/* ****************************************************************************
 * Desc: Print the contents of the library
 * ************************************************************************* */
void print_library(video **library)
{
    if (!library)
        return;

    for (int i = 0; i < VIDEOS; i++)
    {
        printf("Video %i\n", i + 1);

        if (library[i]->title)
            printf("\tTitle: %s\n", library[i]->title);
        if (library[i]->author)
            printf("\tAuthor: %s\n", library[i]->author);
        if (library[i]->link)
            printf("\tLink: %s\n", library[i]->link);
        if (library[i]->likes)
            printf("\tLikes: %i\n", library[i]->likes);
        if (library[i]->date || library[i]->date->month)
            printf("\tUploaded: %s %i, %i\n", library[i]->date->month,
                                            library[i]->date->day,
                                            library[i]->date->year);
    }
    return;
}


/* ****************************************************************************
 * Desc: Free all allocated memory from the library
 * ************************************************************************* */
void free_library(video **library)
{
    for (int i = 0; i < VIDEOS; i++)
    {
        if(library[i]->date)
            free(library[i]->date);
        if(library[i]->title)
            free(library[i]->title);
        if(library[i]->author)
            free(library[i]->author);
        if(library[i]->link)
            free(library[i]->link);
        if(library[i])
            free(library[i]);
    }
    return;
}


/* ****************************************************************************
 * Desc: Function 1 video library using structs
 * ************************************************************************* */
void fun01(void)
{
    video **vids = NULL;

    vids = build_video_library();
    print_library(vids);
    free_library(vids);
    return;
}


/* ****************************************************************************
 * Desc: Function 2 function in function calls
 * ************************************************************************* */
void fun02(void)
{
    int a = 3;
    int b = 5;
    int c;

    c = level1(a, b);
    ++c;
    return;
}


/* ****************************************************************************
 * Desc: Function 3 forking function
 * ************************************************************************* */
void fun03(void)
{
    server();
}


/* ****************************************************************************
 * Desc: Function 4 threads
 * ************************************************************************* */
void fun04(void)
{
    thread();
}


/* ****************************************************************************
 * Desc: Function 5
 * ************************************************************************* */
void fun05(void)
{
    vuln();
}


/* ****************************************************************************
 * Desc: Returns the bug lesson number or zero on error.
 * ************************************************************************* */
int get_input(int argc, char **argv)
{
    if (argv[1][0] == '2') return 2;

    if (argc == 2)
       return atoi(argv[1]);

    return 0;
}


/* ****************************************************************************
 * The main program
 *
 * Details: This function contains the entry point into the various functions.
 *
 * ************************************************************************* */
int main(int argc, char *argv[])
{
    int bug = 0;
    bug = get_input(argc, argv);

    switch(bug)
    {
        case 1: fun01(); break;
        case 2: fun02(); break;
        case 3: fun03(); break;
        case 4: fun04(); break;
        case 5: fun05(); break;
        //case 6: fun06(); break;
        //case 7: fun07(); break;
        //case 8: fun08(); break;
        default:
            printf("Function number %s not recognized\n", argv[1]);
            printf("Usage: %s bugNumber\n", argv[0]);
    }

    return 0;
}

