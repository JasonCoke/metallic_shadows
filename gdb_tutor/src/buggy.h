/*
 * File: buggy.h
 * Description: Buggy header file
 *
 * Details: Description: Holds buggy functions to debug
 *
 */
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


/* ****************************************************************************
 * Desc: Subtract two from a number
 * Buggy function reference: 01
 * Info01:TWlzc2luZyBzZW1pY29sb24gYWZ0ZXIgcmV0dXJuCg==
 *
 * ************************************************************************* */
int subtract_two(int the_number)
{
    int subtract_value;

    printf("The subtract 2 function.\n");

    // Do not subtract if less than zero
    if (the_number < 0)
        return
    // Create the value to subtract
    subtract_value = 2;

    return the_number - subtract_value;
}


/* ****************************************************************************
 * Desc: Sums the values in an array of given length.
 * Buggy function reference: 02
 * Info02:J2knIG5vdCBzZXQgdG8gemVyby4gIEV4dHJhICc7JyBhZnRlciBmb3IgbG9vcC4K
 *
 * ************************************************************************* */
unsigned int sum(int *array, unsigned int elements)
{
    int sum;
    int i;

    printf("The sum function.\n");

    /* Add the sum of array elements and return the value */
    for (i; i < elements; i++); {
        sum += array[i];
    }

    return sum;
}


/* ****************************************************************************
 * Compares the expected array total with the actual total.
 * Buggy function reference: 03
 * Info03:QmFkIGNvbXBhcmUgIj0iIHZzICI9PSIuIE5vIHN3aXRjaCBjYXNlIGJyZWFrJ3MuIAo=
 *
 * ************************************************************************* */
void check_total(unsigned int expected_sum_value, unsigned int actual_sum_value)
{
    bool result;

    /* Compare the expected value with the actual value */
    if (expected_sum_value = actual_sum_value)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    /* Now switch through the result and print the appropriate message */
    switch(result)
    {
        case true:
            printf("The values are the same\n");

        case false:
            printf("The values are the different\n");

        default:
            printf("Something went wrong\n");
    }

    return;
}


/* ****************************************************************************
 * Desc: Updates the value passed in
 * Buggy function reference: 04
 * Info04a:QSB2YWx1ZSBjYW5ub3QgYmUgY2hhbmdlZCB3aXRob3V0IHVzaW5nIGEgcG9pbnRlcgo=
 *
 * ************************************************************************* */
void update_value(int num)
{
    num = num + 100;
    return;
}


/* ****************************************************************************
 * Desc: Updates the value passed in by its pointer
 * Buggy function reference: 04
 * Info04b:UG9pbnRlciBub3QgZGVyZWZlcmVuY2VkCg==
 *
 * ************************************************************************* */
void update_value_by_pointer(int *num)
{
    num = num + 100;
    return;
}


/* ****************************************************************************
 * Desc: Simple loop that adds and subtracts
 * Buggy function reference: 05
 * Info05:SW5maW5hdGUgbG9vcCwgYmFkIGNhbGN1bGF0aW9ucwo=
 *
 * ************************************************************************* */
void loopy(void)
{
    unsigned int num1 = 1;
    unsigned int num2 = 2;

    while(num1 + num2 > 1)
    {
        num1 = 3 - num1;
        num2 = 3 - num2;
    }

    printf("Num1 = %i && Num2 = %i\n", num1, num2);

    return;
}


/* ****************************************************************************
 * Desc: Factorial function that was made buggy
 * Buggy function reference: 06
 * Info06:QmFzZSBjYXNlIG5vdCByZWFjaGFibGUgc2hvdWxkIGJlIGJlZm9yZSBjYWxsCg==
 *
 * ************************************************************************* */
unsigned long long int factorial(unsigned int i)
{
   // Calculation
   return i * factorial(i - 1);

   // Base case to return from recursion
   if(i <= 1) {
      return 1;
   }
}


/* ****************************************************************************
 * Desc: Duplicates a string
 * Buggy function reference: 07
 * Info07:U3RyaW5nIGNvcGllcyBkbyBub3QgY2hlY2sgZm9yIGEgdmFsaWQgc3RyaW5ncwo=
 *
 * ************************************************************************* */
void duplicate_string(char *new, char *old)
{
    // Use string copy
    strcpy(new, old);

    return;
}


/* ****************************************************************************
 * Desc: Compare two given strings
 * Buggy function reference: 08
 * Info08a:TGVuZ3RoIGNvbXBhcmUgZXhjbHVkZXMgcmFuZ2UsICYmIHZzIHx8Cg==
 * Info08b:U3RyaW5nIGNvbXBhcmlzb24gdXNpbmcgPT0gb3BlcmF0b3IsIG5vdCBzdHJjbXAoKQo=
 *
 * ************************************************************************* */
void compare_strings(char *a, char *b)
{
    int len1 = 0;
    int len2 = 0;
    char *count_ptr = NULL;

    count_ptr = a;

    // Count the len of a
    while (*count_ptr != '\0') {
        ++len1;
        ++count_ptr;
    }

    count_ptr = b;

    // Count the length of b
    while (*count_ptr) {
       len2++;
       count_ptr++;
    }

    // Compare lengths
    if (len1 < len2 && len1 > len2)
        printf("These strings are NOT the same length\n");
    else if (len1 == len2)
        printf("These strings are the same length\n");
    else
        printf("Something went wrong with measuring the length\n");

    // Compare strings
    if (a == b)
       printf("The strings are equal\n");
    else
       printf("The strings are unequal\n");

    return;
}


/* ****************************************************************************
 * Desc: Template Function 10 lines
 * Buggy function reference: XX
 * Info1x:
 *
void function_template(void)
{
    return;
}
 * ************************************************************************* */

