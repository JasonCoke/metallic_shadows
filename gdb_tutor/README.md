# Welcome to the GDB Tutor

------------------------------------------------------------------------------
ABOUT
------------------------------------------------------------------------------

The GDB tutorial is a text based series of lessons that allow you to type
and retype GDB commands in order to learn and get experience in debugging
with GDB.


------------------------------------------------------------------------------
SYSTEM REQUIREMENTS:
------------------------------------------------------------------------------

Use a Linux distribution.  The tutorial was developed on a system running
Kali 2020.1 machine using GDB version (Debian 9.2.1) 9.1.  The tutorial
should work on any Linux machine running a similar GDB.

The tutorial was tested on:
  -  Fedora release 33 with GDB 9.2-7


Note: There could be slight differences in the execution of commands if you
don't use the exact version Kali and GDB.



------------------------------------------------------------------------------
HOW TO START:
------------------------------------------------------------------------------

Open the gdb-tutor.txt file in one terminal and use a second terminal to
do the lesson.



------------------------------------------------------------------------------
LIST OF FILES:
------------------------------------------------------------------------------

| File | Description |
| ----- | ----- |
| gdb-tutor.txt           | This is the tutorial |
| gdb-tutor-appendix.txt  | An appendix of the commands used in the tutorial |
| makefile                | Compiles all source code (optional) |
| src                     | Contains the source code used for the tutorial |


