# metallic_shadows

A platform for teaching programming related topics.



## GDB
- Directory: gdb_tutor
- Description: A text based series of lessons that introduce GDB


## Peda-GDB
  - Directory: peda_tutor
  - Description: A text based series of lessons that introduce Peda-GDB


## Vim IDE
  - Directory: vim_ide
  - Description: A demostration of how to turn vim into a IDE
