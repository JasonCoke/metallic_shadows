# Welcome to the GDB PEDA Tutor

## ABOUT

The Peda-GDB tutorial is a text based series of lessons that allow you
to type and retype Peda-GDB commands in order to learn and get experience
in debugging.  Peda-GDB is a grouping of python scripts that are used
within GDB to faciliate/ease exploit development.



## SYSTEM REQUIREMENTS

Use a Linux distribution.  The tutorial was developed on a system running
Kali 2020.1 machine using GDB version (Debian 9.2.1) 9.1.  The tutorial
should work on any Linux machine running a similar GDB.



## HOW TO START

Open the peda-tutor.txt file in one terminal and use a second terminal to
do the lesson.



## LIST OF FILES

| File  | Details |
| ----- | ----- |
| peda-tutor.txt        | This is the tutorial |
| peda-tutor-appendix   | An appendix of the commands used in the tutorial |
| makefile              | Compiles all source code (optional) |
| src                   | Contains the source code used for the tutorial |



