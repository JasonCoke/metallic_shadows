#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void func_first(char *);
void func_second(char *);

int main(void) {
    char *word;
    func_first(word);
    free(word);
    return 0;
}

void func_first(char *w) {
    char x = '0';
    func_second(w);
    x++;
}

void func_second(char *s) {
    char buf[10];
    fgets(buf, 100, stdin);  // Buffer Overflow
}
